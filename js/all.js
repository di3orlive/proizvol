jQuery(function($){

    $(".rslides").responsiveSlides({
        auto: true,             // Boolean: Animate automatically, true or false
        speed: 500,            // Integer: Speed of the transition, in milliseconds
        timeout: 5000,          // Integer: Time between slide transitions, in milliseconds
        pager: false,           // Boolean: Show pager, true or false
        nav: false,             // Boolean: Show navigation, true or false
        random: false,          // Boolean: Randomize the order of the slides, true or false
        pause: false,           // Boolean: Pause on hover, true or false
        pauseControls: true,    // Boolean: Pause when hovering controls, true or false
        prevText: "Previous",   // String: Text for the "previous" button
        nextText: "Next",       // String: Text for the "next" button
        maxwidth: "",           // Integer: Max-width of the slideshow, in pixels
        navContainer: "",       // Selector: Where controls should be appended to, default is after the 'ul'
        manualControls: "",     // Selector: Declare custom pager navigation
        namespace: "rslides",   // String: Change the default namespace used
        before: function(){},   // Function: Before callback
        after: function(){}     // Function: After callback
    });

    $('.bxslider').bxSlider({
        nextText: "",
        prevText: "",
        speed: 1000,
        auto: true,
        pause: 10000,
        pager: false,
        autoHover: true
    });

    $('.phonefield').mask('9 (999) 999-99-99');

    if ($(window).width() >= 960) {
        $('.animate-box').addClass("hidden").viewportChecker({
            classToAdd: 'visible animated bounceInLeft', // Class to add to the elements when they are visible
            offset: 50
        });

        $('.animate-box2').addClass("hidden").viewportChecker({
            classToAdd: 'visible animated bounceInRight', // Class to add to the elements when they are visible
            offset: 50
        });

        $('.animate-box3').addClass("hidden").viewportChecker({
            classToAdd: 'visible animated bounceIn', // Class to add to the elements when they are visible
            offset: 50
        });
    }

});
